# eslint-config-node

Origin address :https://github.com/zeit/eslint-config-node

Record Issue:
1、Parsing error: 'import' and 'export' may appear only with 'sourceType: module'

    "parserOptions": {
        "sourceType": "module"
    }
 

Contains a series of sensible defaults on top of the eslint:recommended config to prevent common JavaScript gotchas.

It explicitly avoids and unsets any stylistic rules to be as free of opinions as possible.

## How to use

Just run...

```
$ yarn add --dev eslint @zeit/eslint-config-node @zeit/git-hooks
$ yarn zeit-lint-init

```
... and you're (pre)set!